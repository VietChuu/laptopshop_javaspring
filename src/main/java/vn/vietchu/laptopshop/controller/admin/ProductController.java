package vn.vietchu.laptopshop.controller.admin;

import java.util.List;
import java.util.Optional;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.validation.FieldError;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.multipart.MultipartFile;

import jakarta.validation.Valid;
import vn.vietchu.laptopshop.domain.Product;
import vn.vietchu.laptopshop.service.ProductService;
import vn.vietchu.laptopshop.service.UploadService;

@Controller
public class ProductController {

    private final UploadService uploadService;
    private final ProductService productService;

    public ProductController(UploadService uploadService, ProductService productService) {

        this.uploadService = uploadService;
        this.productService = productService;

    }

    // @GetMapping("/admin/product") // GET
    // public String getUserPage(Model model, @RequestParam(value = "page",
    // defaultValue = "1") int page) {
    // Pageable pageable = PageRequest.of(page - 1, 5);
    // Page<Product> prods = this.productService.fetchProducts(pageable); // lay
    // data
    // List<Product> listProducts = prods.getContent();
    // model.addAttribute("prods", listProducts);
    // model.addAttribute("currentPage", page);
    // model.addAttribute("totalPages", prods.getTotalPages());
    // return "admin/product/show";
    // }

    @GetMapping("/admin/product")
    public String getProduct(
            Model model,
            @RequestParam("page") Optional<String> pageOptional) {
        int page = 1;
        try {
            if (pageOptional.isPresent()) {
                // convert from String to int
                page = Integer.parseInt(pageOptional.get());
            } else {
                // page = 1
            }
        } catch (Exception e) {
            // page = 1
            // TODO: handle exception
        }
        Pageable pageable = PageRequest.of(page - 1, 5);
        Page<Product> prs = this.productService.fetchProducts(pageable);
        List<Product> listProducts = prs.getContent();
        model.addAttribute("prods", listProducts);
        model.addAttribute("currentPage", page);
        model.addAttribute("totalPages", prs.getTotalPages());

        return "admin/product/show";
    }

    @GetMapping("/admin/product/create") // GET
    public String getCreateProductPage(Model model) {
        model.addAttribute("newProduct", new Product());
        return "admin/product/create";
    }

    @PostMapping("/admin/product/create")
    public String getCreateProduct(
            @ModelAttribute("newProduct") @Valid Product prod, BindingResult newProductBindingResult,
            @RequestParam("prodFile") MultipartFile file) {
        List<FieldError> errors = newProductBindingResult.getFieldErrors();
        for (FieldError error : errors) {
            System.out.println(error.getField() + " - " + error.getDefaultMessage());
        }
        // validate
        if (newProductBindingResult.hasErrors()) {
            return "/admin/product/create";
        }
        String image = this.uploadService.handleSaveUploadFile(file, "product");
        prod.setImage(image);
        this.productService.createProduct(prod);
        return "redirect:/admin/product"; // redirect dung de chuyen huong duong dan
    }

    @GetMapping("/admin/product/{id}") // GET
    public String getProductDetailPage(Model model, @PathVariable long id) {
        Product prods = this.productService.fetchProductById(id).get();
        model.addAttribute("pr", prods);
        model.addAttribute("id", id);
        return "admin/product/productDetail";
    }

    @GetMapping("/admin/product/delete/{id}") // GET
    public String getDeleteProduct(Model model, @PathVariable long id) {
        model.addAttribute("id", id);
        model.addAttribute("newProduct", new Product());
        return "admin/product/deleteProduct";
    }

    @PostMapping("/admin/product/delete")
    public String postDeleteProduct(@ModelAttribute("newUser") Product prod) {
        this.productService.deleteProduct(prod.getId());
        return "redirect:/admin/product";
    }

    @GetMapping("/admin/product/update/{id}") // GET
    public String getUpdateProductPage(Model model, @PathVariable long id) {
        Product currentProduct = this.productService.fetchProductById(id).get();
        model.addAttribute("newProduct", currentProduct);
        return "admin/product/productUpdate";
    }

    @PostMapping("/admin/product/update")
    public String postUpdateProduct(@ModelAttribute("newProduct") @Valid Product prod,
            BindingResult newProductBindingResult,
            @RequestParam("vietchuFile") MultipartFile file) {

        // validate
        if (newProductBindingResult.hasErrors()) {
            return "admin/product/productUpdate";
        }
        Product currentProduct = this.productService.fetchProductById(prod.getId()).get();

        if (currentProduct != null) {
            if (!file.isEmpty()) {
                String img = this.uploadService.handleSaveUploadFile(file, "product");
                currentProduct.setImage(img);
            }
            currentProduct.setName(prod.getName());
            currentProduct.setPrice(prod.getPrice());
            currentProduct.setDetailDesc(prod.getDetailDesc());
            currentProduct.setShortDesc(prod.getShortDesc());
            currentProduct.setFactory(prod.getFactory());
            currentProduct.setTarget(prod.getTarget());
            currentProduct.setQuantity(prod.getQuantity());

            this.productService.createProduct(currentProduct);

        }
        return "redirect:/admin/product";
    }
}
