<%@page contentType="text/html" pageEncoding="UTF-8" %>
    <%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
        <footer class="py-4 bg-light mt-auto">
            <div class="container-fluid px-4">
                <div class="d-flex align-items-center justify-content-between small">
                    <div class="text-muted">Copyright &copy;</div>
                    <div>
                        <a href="" target="_blank">Website</a>
                        &middot;
                        <a href="https://www.facebook.com/profile.php?id=100009242636123" target="_blank">Facebook</a>
                    </div>
                </div>
            </div>
        </footer>